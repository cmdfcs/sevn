.. SEVNy documentation master file, created by
   sphinx-quickstart on Sun Jul  9 20:13:49 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SEVNpy's documentation!
===================================



*****************
Table of Contents
*****************

.. toctree::
   :maxdepth: 2

   getting_started/index
   api/index