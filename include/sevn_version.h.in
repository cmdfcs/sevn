//
// Created by iorio on 1/27/23.
//

#ifndef SEVN_SEVN_VERSION_H
#define SEVN_SEVN_VERSION_H

#include <string>

struct SEVNinfo{

    static constexpr unsigned int VERSION_MAJOR     =  @SEVN_MAJOR_VERSION@;
    static constexpr unsigned int VERSION_MINOR     =  @SEVN_MINOR_VERSION@;
    static constexpr unsigned int VERSION_PATCH     =  @SEVN_PATCH_VERSION@;
    static constexpr auto VERSION    = "@SEVN_VERSION_STRING@";
    static constexpr auto DEVLINE    = "@SEVN_DEVELOPMENT_LINE@";

    //GIT BRANCH Name
    static constexpr auto GIT_BRANCH                = "@GIT_BRANCH@";

    //GIT Local HEAD
    //These are the info about the local repo not yet pushed to the remote branch
    static constexpr auto GIT_SHA                                = "@GIT_SHA_LOCAL@";
    static constexpr auto GIT_SHATIME                            = "@GIT_SHATIME_LOCAL@";
    static constexpr unsigned long int GIT_SHATIMESTAMP          = @GIT_SHATIMESTAMP_LOCAL@;
	static constexpr unsigned int GIT_COUNTER                    = @GIT_COUNTER_LOCAL@;

	//GIT Local remote HEAD
	//These are the info about the last updates on the  remote branch
    static constexpr auto GIT_SHA_REMOTE                                = "@GIT_SHA_REMOTE@";
    static constexpr auto GIT_SHATIME_REMOTE                            = "@GIT_SHATIME_REMOTE@";
    static constexpr unsigned long int GIT_SHATIMESTAMP_REMOTE          = @GIT_SHATIMESTAMP_REMOTE@;
	static constexpr unsigned int GIT_COUNTER_REMOTE                    = @GIT_COUNTER_REMOTE@;

    static std::string get_full_info(){

        std::string full_info;
        full_info+="*********************\n";
        full_info+="SEVN version info:\n";
        full_info+="---------------------\n";
        full_info+=" SEVN DEVELOPMENT LINE:" + std::string(SEVNinfo::DEVLINE);
        full_info+="\n SEVN VERSION:" + std::string(SEVNinfo::VERSION);
        full_info+="\n SEVN BRANCH:" + std::string(SEVNinfo::GIT_BRANCH);
        full_info+="\n---------------------\n";
        full_info+="Git local info:";
        full_info+="\n COMMITS COUNTER:" + std::to_string(SEVNinfo::GIT_COUNTER);
        full_info+="\n LAST COMMIT HASH:" + std::string(SEVNinfo::GIT_SHA);
        full_info+="\n LAST COMMIT DATE:" + std::string(SEVNinfo::GIT_SHATIME)  + " (TIMESTAMP: "+std::to_string(SEVNinfo::GIT_SHATIMESTAMP)+")";
        full_info+="\nGit remote info:";
        full_info+="\n COMMITS COUNTER:" + std::to_string(SEVNinfo::GIT_COUNTER_REMOTE);
        full_info+="\n LAST COMMIT HASH:" + std::string(SEVNinfo::GIT_SHA_REMOTE);
        full_info+="\n LAST COMMIT DATE:" + std::string(SEVNinfo::GIT_SHATIME_REMOTE)  + " (TIMESTAMP: "+std::to_string(SEVNinfo::GIT_SHATIMESTAMP_REMOTE)+")";
        full_info+="\n*********************\n";

        return full_info;
    }
};

#endif //SEVN_SEVN_VERSION_H
