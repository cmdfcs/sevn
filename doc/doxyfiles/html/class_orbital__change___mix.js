var class_orbital__change___mix =
[
    [ "Orbital_change_Mix", "class_orbital__change___mix.html#aba421175bfea5ce897fe43affe8c70ca", null ],
    [ "~Orbital_change_Mix", "class_orbital__change___mix.html#a0357323d592be5ae9c08f4d79c868171", null ],
    [ "DA", "class_orbital__change___mix.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "class_orbital__change___mix.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_orbital__change___mix.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "class_orbital__change___mix.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "class_orbital__change___mix.html#a71424b6ad0b121dc3cb81e939845b081", null ],
    [ "GetUsed", "class_orbital__change___mix.html#a0574e56ec9f5c67cd57695bacc036ead", null ],
    [ "init", "class_orbital__change___mix.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_orbital__change___mix.html#a08b2155c96305a411467e6fe791cdf06", null ],
    [ "Instance", "class_orbital__change___mix.html#a4ab9369e688d5b89dc892f816b1932d7", null ],
    [ "is_process_ongoing", "class_orbital__change___mix.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_orbital__change___mix.html#a09e657632c0cf090be5dc93bb3fc09db", null ],
    [ "Register", "class_orbital__change___mix.html#a59a35de94c46c3eeabae46c6a668e764", null ],
    [ "set_options", "class_orbital__change___mix.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_orbital__change___mix.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "svlog", "class_orbital__change___mix.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];