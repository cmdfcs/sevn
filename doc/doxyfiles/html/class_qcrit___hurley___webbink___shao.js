var class_qcrit___hurley___webbink___shao =
[
    [ "Qcrit_Hurley_Webbink_Shao", "class_qcrit___hurley___webbink___shao.html#a47a1029947ff0ccbdf9f5feb287d285a", null ],
    [ "get", "class_qcrit___hurley___webbink___shao.html#ad8fe79c8091329b1d629bae45bf82b19", null ],
    [ "get", "class_qcrit___hurley___webbink___shao.html#a5bc35cce7d8911dc44ab2adb041f070b", null ],
    [ "get_tshold", "class_qcrit___hurley___webbink___shao.html#af332e0c7bf6949c9950a1eb31580530f", null ],
    [ "get_tshold", "class_qcrit___hurley___webbink___shao.html#aaaffcc6356c440afd5a5fa3db97b0a52", null ],
    [ "GetStaticMap", "class_qcrit___hurley___webbink___shao.html#af82526c9ac590ef5be1f249862067184", null ],
    [ "GetUsed", "class_qcrit___hurley___webbink___shao.html#a2d0588da5e95cad333c236ea04f738fb", null ],
    [ "instance", "class_qcrit___hurley___webbink___shao.html#a35b7af3f00017390e4622d3eba1637ef", null ],
    [ "Instance", "class_qcrit___hurley___webbink___shao.html#a1352dbce732d5ee8210358cfa6191631", null ],
    [ "mt_unstable", "class_qcrit___hurley___webbink___shao.html#ae46bbb474be37c75419d85e1fde7edf2", null ],
    [ "name", "class_qcrit___hurley___webbink___shao.html#a11c18afbb944ac75955d5905b050d40e", null ],
    [ "q", "class_qcrit___hurley___webbink___shao.html#a5aa7075e6a61ee73d8694c84305a9222", null ],
    [ "qcrit", "class_qcrit___hurley___webbink___shao.html#a1e0ca88b087bae9b2d0f7e391134f4cf", null ],
    [ "qcrit_giant", "class_qcrit___hurley___webbink___shao.html#ad9bb5a12cb146fc52a328e249b02a803", null ],
    [ "Register", "class_qcrit___hurley___webbink___shao.html#a8172b95b56ad29b5d31eaf2499b24317", null ],
    [ "_qcrit_hurley", "class_qcrit___hurley___webbink___shao.html#af746a8c2e33be5f74b22e4d55cf5e7fe", null ],
    [ "_qcrit_hurley_webbink", "class_qcrit___hurley___webbink___shao.html#a323f9035a0637bdd7349883d8245154c", null ],
    [ "_qcrit_hurley_webbink_shao", "class_qcrit___hurley___webbink___shao.html#abdc08aaea1fcd9e115c191ee3c4dd361", null ],
    [ "svlog", "class_qcrit___hurley___webbink___shao.html#a68e0d337f38df758c90280a2832b0000", null ]
];