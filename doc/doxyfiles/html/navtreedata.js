/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "SEVN", "index.html", [
    [ "News", "index.html#autotoc_md0", null ],
    [ "Quickstart", "index.html#autotoc_md1", [
      [ "Requirements", "index.html#autotoc_md2", [
        [ "Conda environments", "index.html#autotoc_md3", null ]
      ] ],
      [ "Installation", "index.html#autotoc_md4", [
        [ "Compile script", "index.html#autotoc_md5", null ],
        [ "Cmake", "index.html#autotoc_md6", null ]
      ] ],
      [ "Run", "index.html#autotoc_md7", [
        [ "Run scripts", "index.html#autotoc_md8", null ]
      ] ]
    ] ],
    [ "Documentation", "index.html#autotoc_md9", null ],
    [ "Support", "index.html#autotoc_md10", null ],
    [ "Contributing", "index.html#autotoc_md11", null ],
    [ "Authors and acknowledgment", "index.html#autotoc_md12", null ],
    [ "License", "index.html#autotoc_md13", null ],
    [ "Developers guide", "md_devepguide.html", null ],
    [ "FAQ", "md__f_a_q.html", null ],
    [ "WIKI", "md__wiki.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_b_s_eintegrator_8h.html",
"class_b_j_i_t___property.html#a56b0e563f1deae887de4ed117914e346",
"class_b_worldtime.html#a6ee50b550adecd1e2961f401e4478198",
"class_c_c15.html#aaad1d0c62bd257c7515dcf6db8ee03f4",
"class_circularisation_periastron_full.html#a8af93dedd4c8c423005349fef53e17aa",
"class_core_radius.html#a30d86c7b7e739b1afc7a4d53cc9eea28",
"class_depthconv.html#a7c2a7b00c6288012f1a9d406cd05d22e",
"class_e_c_u_s30.html#a90ef65680bb5a84681b02b9939cecbe9",
"class_empty.html#a86f70b8787f98515ecef4505218ca863",
"class_g_wtime.html#a01bc98fce22cf5f8891536b64a77af85",
"class_hardening_fast_cluster.html#ad89d46b229414c2cd8077312b06e06cc",
"class_hurley__mod__rl.html#aa348c37d1e944257eab4d969d9529bab",
"class_hurley__rl__propeller.html#a3cf29e9a32155ff44d74e1313cda8d07",
"class_inertia.html#af3b5eb999c4a8ad34605a641d6acfd35",
"class_kollision_hurley.html#ae9e6f863b7cbba6d89b8f4ec58d6a884",
"class_localtime.html#ab3633c8002ac35837a1a68f668c14ed2",
"class_m_h_e.html#adce76cada3a5f0fca7df313ce4b0766e",
"class_mass__obejct.html#a805296cb4e8e852b9df907c20e4baff5",
"class_n_srem.html#ac7592dbfe124d980ffa2589c23a54649",
"class_nsup.html#a51e9d3a2e917dbf2fce5778d14fe7231",
"class_optional_table_property.html#a50ef56abe329da594c8cf7629f3243ce",
"class_orbital__change___tides.html#aeed36f4b270c5771f03936e7f8cce83a",
"class_phase.html#a585894846bb9cadf7e2d73f13ec6cbf1",
"class_plife.html#a55bb13c2b1ee36882c549d9cf37e2df0",
"class_qconv.html#afb2f76dec040c60ff20fec8fa7437363",
"class_r_c_o.html#a3bbffe0d9bfc1c9761fb8936a6ee6f63",
"class_radius.html#a769ea3b1ce48aa6445d3123e3f5a6d01",
"class_s_e_v_npar.html",
"class_standard_circularisation.html#aa3d88fef38cee969b3d94db346195958",
"class_star.html#ad38bfb6623cc3bdf4650357355db29d8",
"class_star__auxiliary.html#ac9b461b8470fac0f4cf5359ef564d335",
"class_tconv.html#a1f1ea7b540ab7e300d09e4b7d02e5d34",
"class_tides__simple.html#aeed36f4b270c5771f03936e7f8cce83a",
"class_w_drem.html#ab1a74d09d4b48efdddce170bbd06c1f8",
"class_worldtime.html#afee788201ce0d476de3656152763edbe",
"class_zombierem.html#a106e202d643ebb95b80757ba680a83ee",
"classd_m_r_l_odt.html#a55bb13c2b1ee36882c549d9cf37e2df0",
"classd_mcumul__binary.html#ab5cdf06d67068eb1098883cfffe31581",
"classdadt.html#a4b3e375f099d94704522bc6277783e90",
"classdisabled___s_n_kicks.html#a3d392dd57fcfa5422b9ddf87047449cc",
"classevolve__utility_1_1_evolve_b_h_m_s.html#a2356fd448fb3b566eae418728ad961ed",
"classevolve__utility_1_1_evolve_functor.html#ad66d499476c18572ad91aa081f72565b",
"classrapid__gau_n_s.html#ac5b20357ddaace84748d3820381db426",
"classsimple__mix.html#a3425194ee18f327bbe9751aaca12df1b",
"hobbs_8h_source.html",
"namespace_lookup.html#a55966f8991a50ee053a9ba892fa90916",
"namespaceutilities.html#abc62a2b904b601c04ed9c5835db161c4",
"utilities_8h.html#ab6872b4710c09a5803080889cbde042b"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';