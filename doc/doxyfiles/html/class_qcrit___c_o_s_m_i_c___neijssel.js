var class_qcrit___c_o_s_m_i_c___neijssel =
[
    [ "Qcrit_COSMIC_Neijssel", "class_qcrit___c_o_s_m_i_c___neijssel.html#a5c23b080d19a2750dd64347152af9188", null ],
    [ "get", "class_qcrit___c_o_s_m_i_c___neijssel.html#ad8fe79c8091329b1d629bae45bf82b19", null ],
    [ "get", "class_qcrit___c_o_s_m_i_c___neijssel.html#a5bc35cce7d8911dc44ab2adb041f070b", null ],
    [ "get_tshold", "class_qcrit___c_o_s_m_i_c___neijssel.html#af332e0c7bf6949c9950a1eb31580530f", null ],
    [ "get_tshold", "class_qcrit___c_o_s_m_i_c___neijssel.html#aaaffcc6356c440afd5a5fa3db97b0a52", null ],
    [ "GetStaticMap", "class_qcrit___c_o_s_m_i_c___neijssel.html#af82526c9ac590ef5be1f249862067184", null ],
    [ "GetUsed", "class_qcrit___c_o_s_m_i_c___neijssel.html#a2d0588da5e95cad333c236ea04f738fb", null ],
    [ "instance", "class_qcrit___c_o_s_m_i_c___neijssel.html#afba1300c7abb588ecc723909813b0dbc", null ],
    [ "Instance", "class_qcrit___c_o_s_m_i_c___neijssel.html#a1352dbce732d5ee8210358cfa6191631", null ],
    [ "mt_unstable", "class_qcrit___c_o_s_m_i_c___neijssel.html#ae46bbb474be37c75419d85e1fde7edf2", null ],
    [ "name", "class_qcrit___c_o_s_m_i_c___neijssel.html#a70891cdba06732453c6aa3717e395250", null ],
    [ "q", "class_qcrit___c_o_s_m_i_c___neijssel.html#a5aa7075e6a61ee73d8694c84305a9222", null ],
    [ "qcrit", "class_qcrit___c_o_s_m_i_c___neijssel.html#a41353fbfbe98de9ba7fdf38de30c711d", null ],
    [ "qcrit_giant", "class_qcrit___c_o_s_m_i_c___neijssel.html#ad9bb5a12cb146fc52a328e249b02a803", null ],
    [ "Register", "class_qcrit___c_o_s_m_i_c___neijssel.html#a8172b95b56ad29b5d31eaf2499b24317", null ],
    [ "_qcrit_cosmic_neijssel", "class_qcrit___c_o_s_m_i_c___neijssel.html#a9aaa89933c8e7fe817bb6fb8bf970308", null ],
    [ "_qcrit_hurley", "class_qcrit___c_o_s_m_i_c___neijssel.html#af746a8c2e33be5f74b22e4d55cf5e7fe", null ],
    [ "_qcrit_hurley_webbink", "class_qcrit___c_o_s_m_i_c___neijssel.html#a323f9035a0637bdd7349883d8245154c", null ],
    [ "svlog", "class_qcrit___c_o_s_m_i_c___neijssel.html#a68e0d337f38df758c90280a2832b0000", null ]
];