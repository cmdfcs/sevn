var class_orbital__change =
[
    [ "Orbital_change", "class_orbital__change.html#a7bc966725b8d4fc8cd807cfd7b410a2f", null ],
    [ "~Orbital_change", "class_orbital__change.html#af9ad1b26193c988f3f6b2303f9f8364f", null ],
    [ "Orbital_change", "class_orbital__change.html#adf2244e62bcf25ab4478dfa5f6498aa1", null ],
    [ "Orbital_change", "class_orbital__change.html#a4861e62d5cb2b5417e78db660d321644", null ],
    [ "DA", "class_orbital__change.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "class_orbital__change.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_orbital__change.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "class_orbital__change.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "init", "class_orbital__change.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "is_process_ongoing", "class_orbital__change.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_orbital__change.html#ab24ef0207a5065c4aa8376a0478b2504", null ],
    [ "operator=", "class_orbital__change.html#a64d1b9d4c1e505e47d0f3ccdf9a6f168", null ],
    [ "set_options", "class_orbital__change.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_orbital__change.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "svlog", "class_orbital__change.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];