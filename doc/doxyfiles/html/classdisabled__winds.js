var classdisabled__winds =
[
    [ "disabled_winds", "classdisabled__winds.html#a0dea7c81a3af17de506d69a8b57b7b9a", null ],
    [ "DA", "classdisabled__winds.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "classdisabled__winds.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "classdisabled__winds.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "classdisabled__winds.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "classdisabled__winds.html#a2f8118fb28c939655f9a1dff47f73a3d", null ],
    [ "GetUsed", "classdisabled__winds.html#a167f5aff25188a471f3ce86e061fe9dc", null ],
    [ "init", "classdisabled__winds.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "classdisabled__winds.html#a21c58d5ccdd8fe2dbfb7422870e38e64", null ],
    [ "Instance", "classdisabled__winds.html#a9b10015bec8cf096e139260c5eee4f6b", null ],
    [ "is_process_ongoing", "classdisabled__winds.html#ad0c9613490aa8d57239048110f78d0e0", null ],
    [ "name", "classdisabled__winds.html#ade065395525d0939d55d5644f042d996", null ],
    [ "Register", "classdisabled__winds.html#aa8b054420b1078c9200866fdc5b79a1c", null ],
    [ "set_options", "classdisabled__winds.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "classdisabled__winds.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "_disabled_winds", "classdisabled__winds.html#a0f2573cbb00eeae04296296ee8d12287", null ],
    [ "svlog", "classdisabled__winds.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];