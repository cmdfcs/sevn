var class_b_hrem =
[
    [ "BHrem", "class_b_hrem.html#a838bd6048bcfdc1d2b1a70b93615809b", null ],
    [ "BHrem", "class_b_hrem.html#a4073b52d929438a7c5bf27abc179f39c", null ],
    [ "age", "class_b_hrem.html#a81d90357225dd7b0e189413347733e8e", null ],
    [ "apply_Bavera_correction_to_Xspin", "class_b_hrem.html#aad237b960887a9ac55a3142fc045532b", null ],
    [ "Bmag", "class_b_hrem.html#a88bae272c6f8b18636556b7be8b75747", null ],
    [ "default_initialiser", "class_b_hrem.html#a2b36961f3325a2918f4a4bae03a9d495", null ],
    [ "estimate_Xspin", "class_b_hrem.html#ad6e488b041bf08548ff676e7c9f1baa2", null ],
    [ "get", "class_b_hrem.html#a4f0a63fddb7ee138f5c494af44132312", null ],
    [ "get_born_time", "class_b_hrem.html#a3fb2eec3b62247ddec79d9256b3f6dfd", null ],
    [ "get_Mremnant_at_born", "class_b_hrem.html#ac7592dbfe124d980ffa2589c23a54649", null ],
    [ "get_remnant_type", "class_b_hrem.html#af172e833f7a2be034177e5ec3546760c", null ],
    [ "Inertia", "class_b_hrem.html#aace4b119ae4308e2c1e8462c79ffbf65", null ],
    [ "InertiaSphere", "class_b_hrem.html#ab1a74d09d4b48efdddce170bbd06c1f8", null ],
    [ "Luminosity", "class_b_hrem.html#ae99c7edfb86c5a94f486c88d620c5483", null ],
    [ "Mass", "class_b_hrem.html#ae1217a0b00f428114665550c1acfaa23", null ],
    [ "OmegaRem", "class_b_hrem.html#a1960f6ff65c2f01676268cf6b187fe70", null ],
    [ "Radius", "class_b_hrem.html#a9735b925f3cc3845467c7ede9927d149", null ],
    [ "set_Xspin", "class_b_hrem.html#aa593e9af7a22b0c9b7068a3bc4f96c54", null ],
    [ "Xspin", "class_b_hrem.html#ad1802f0bf19368bd23779c5a19be8c8e", null ],
    [ "XspinAccretion", "class_b_hrem.html#a0a5ce27a0fc42e25bc96a5f02b117ce9", null ],
    [ "XspinFuller", "class_b_hrem.html#a245e695a51018a83c0a648cf331ae8f4", null ],
    [ "XspinGeneva", "class_b_hrem.html#a2fc9babe34c201d857455adcf253737a", null ],
    [ "XspinMaxwellian", "class_b_hrem.html#a73cb8bb98c23f6daade795cb58e82b3c", null ],
    [ "XspinMESA", "class_b_hrem.html#a07dfc353df2f32fb42a9724d47c88b42", null ],
    [ "XspinZeros", "class_b_hrem.html#a3a03faf7555437763406c4e61e4f2898", null ],
    [ "born_time", "class_b_hrem.html#a9b8ae00c6b853ca8420f2a6fa09950f8", null ],
    [ "Mremnant_at_born", "class_b_hrem.html#a15460ef0ce7a97c3eea8b159ea9ff94a", null ],
    [ "remnant_type", "class_b_hrem.html#a1f2ee7c0a66aabc56e7f038fcc97e033", null ],
    [ "svlog", "class_b_hrem.html#a5d7579476b76d081c413d00b07595c67", null ],
    [ "xspin", "class_b_hrem.html#adf144b2d7bd6ac94b88ce891626efba1", null ]
];