var class_tbgb =
[
    [ "Tbgb", "class_tbgb.html#ac6996e887730814833e71ecae64b25d2", null ],
    [ "eval", "class_tbgb.html#a796ffa171b3c4fbf2c5a41078ccc1444", null ],
    [ "operator()", "class_tbgb.html#acdf2ac0b8cfe4917e1a6e02165228054", null ],
    [ "units", "class_tbgb.html#a74f74d626cc180a06366b33d557aae02", null ],
    [ "a1", "class_tbgb.html#aa19b3ac98db058fbc7ab8d3cb84c8218", null ],
    [ "a2", "class_tbgb.html#ad29a4d74932f4a3e38561b9c8f5a44bc", null ],
    [ "a3", "class_tbgb.html#aa26e773cc561edbc83e44d61a67cac52", null ],
    [ "a4", "class_tbgb.html#a24e00ae531b2af2b88593616514b2056", null ],
    [ "a5", "class_tbgb.html#a54bf441a653ecee87ee24b5d6c398d7a", null ],
    [ "chaced_logP", "class_tbgb.html#ab1e7143a7fd9cfd10eaceaaf2e8edf02", null ],
    [ "chaced_M", "class_tbgb.html#a0cf80f56c1319bd78579e7a1aa94ceef", null ],
    [ "coeff", "class_tbgb.html#a22c53ea104f6abdd1141ca332aea4f4d", null ],
    [ "Z", "class_tbgb.html#add15815038ec1bed19d066ccb1379d64", null ]
];