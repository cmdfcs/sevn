var class_hobbs_pure =
[
    [ "HobbsPure", "class_hobbs_pure.html#a7147caf6d2cd492c0beccd2126045caf", null ],
    [ "_apply", "class_hobbs_pure.html#a5574ee61f5450621cbbf4b851aadc319", null ],
    [ "_apply", "class_hobbs_pure.html#af8176c95e2e8fc9dbb4c2b2ca6440abc", null ],
    [ "apply", "class_hobbs_pure.html#a24df9d565af3b8a2c37575cfd152e561", null ],
    [ "check_and_correct_vkick", "class_hobbs_pure.html#ad579aeb54a9bc522bc03b7afd3229525", null ],
    [ "draw_from_gaussian", "class_hobbs_pure.html#abcdf2feb02d48058575eb7fa1ce19b51", null ],
    [ "get_random_kick", "class_hobbs_pure.html#a8f2cbe6992b73bd4909aecefa857a09a", null ],
    [ "GetStaticMap", "class_hobbs_pure.html#add42458ac89c18dd5b4ccdb572aec0e0", null ],
    [ "GetUsed", "class_hobbs_pure.html#a8af03dddc996c29395b57e0b41abf2f8", null ],
    [ "instance", "class_hobbs_pure.html#ace3c9ea942e690921f11efaffd9472bb", null ],
    [ "Instance", "class_hobbs_pure.html#a0bcf9d165b436dceef7b3636600f9750", null ],
    [ "kick_initializer", "class_hobbs_pure.html#ad1d51caa463f9268cbf1c349e3ee05a0", null ],
    [ "name", "class_hobbs_pure.html#ad51a7cf74882ce596ba6b4693cf096de", null ],
    [ "Register", "class_hobbs_pure.html#a86dff21954db932117e2b4ff1146a192", null ],
    [ "set_random_kick", "class_hobbs_pure.html#a29029dc8666af30104c1483c60c1ac7e", null ],
    [ "_hobbspure", "class_hobbs_pure.html#a02e2f0fabe0d955ee3069296d6a049a7", null ],
    [ "random_velocity_kick", "class_hobbs_pure.html#aa489fda64b125b3465f9f6249de25b83", null ],
    [ "standard_gaussian", "class_hobbs_pure.html#ae1056779a3fc3a09c414618b5c61eb92", null ],
    [ "svlog", "class_hobbs_pure.html#a90ef65680bb5a84681b02b9939cecbe9", null ],
    [ "uniformRealDistribution", "class_hobbs_pure.html#af209b3d8f2caa3fc991c5f53b6f1f0d3", null ]
];