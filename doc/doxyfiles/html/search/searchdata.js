var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "_abcdeghijklmnopqrstuwxz",
  2: "elsu",
  3: "bcdefghiklmnoprstuwz",
  4: "_abcdefghijklmnopqrstuwxz~",
  5: "_abcdefghijklmnoprstuvwxyz",
  6: "_bcefgijmnorstw",
  7: "_cdegimoprstw",
  8: "_bceghimnopqrstuwz",
  9: "bo",
  10: "_dfnops",
  11: "dfsw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

