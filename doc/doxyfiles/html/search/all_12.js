var searchData=
[
  ['r_5falfven_0',['R_Alfven',['../namespaceutilities.html#a3f43c17cef43eb800adb11f8149fb8fa',1,'utilities']]],
  ['r_5fcore_5fprimary_1',['R_core_primary',['../class_common_envelope.html#a7f4ab792029896ea0aae1c4f5168c58a',1,'CommonEnvelope']]],
  ['r_5fcore_5fsecondary_2',['R_core_secondary',['../class_common_envelope.html#a77558f340722dc3bea95373fa67d0adb',1,'CommonEnvelope']]],
  ['r_5fobject_3',['R_object',['../class_r__object.html',1,'']]],
  ['r_5fschwarzschild_4',['R_Schwarzschild',['../namespaceutilities.html#ab0f49bc367ee398c0d0c743e2885fc3c',1,'utilities']]],
  ['radius_5',['Radius',['../class_radius.html',1,'Radius'],['../class_radius.html#a46c4b83e4e3e1f2e569fc83b0b36b52a',1,'Radius::Radius()'],['../class_staremnant.html#ad9f236c20b73c9b786d2e7c135a2d219',1,'Staremnant::Radius()'],['../class_b_hrem.html#a9735b925f3cc3845467c7ede9927d149',1,'BHrem::Radius()'],['../class_n_srem.html#a11797f4fcde5aa8131279774fb12b51a',1,'NSrem::Radius()'],['../class_w_drem.html#a629a5f356fbca674c95b2baaae6df299',1,'WDrem::Radius()'],['../class_zombierem.html#a5d9641f3b447d4fd9ff1d939f1f400b7',1,'Zombierem::Radius(_UNUSED Star *s) const override']]],
  ['radius_6',['radius',['../class_zombierem.html#a106e202d643ebb95b80757ba680a83ee',1,'Zombierem']]],
  ['radx_7',['Radx',['../class_binstar.html#a474c1c11e647c7271bcf5358f48e27ab',1,'Binstar']]],
  ['rand_5funif_5f0_5f1_8',['rand_unif_0_1',['../classcompactness.html#ac69bd113c2778f21dd6840195ec4dddf',1,'compactness']]],
  ['random_5fkeygen_9',['random_keygen',['../namespaceutilities.html#a78da5ab9c9281a8a6a7e75e26c3083db',1,'utilities']]],
  ['random_5fvelocity_5fkick_10',['random_velocity_kick',['../class_kicks.html#aa489fda64b125b3465f9f6249de25b83',1,'Kicks']]],
  ['rapid_11',['rapid',['../classrapid.html',1,'rapid'],['../classrapid.html#a150752f4c120381bae3410be468b94aa',1,'rapid::rapid()']]],
  ['rapid_2ecpp_12',['rapid.cpp',['../rapid_8cpp.html',1,'']]],
  ['rapid_2eh_13',['rapid.h',['../rapid_8h.html',1,'']]],
  ['rapid_5fgauns_14',['rapid_gauNS',['../classrapid__gau_n_s.html',1,'rapid_gauNS'],['../classrapid__gau_n_s.html#a797a07aaee47ddd270713a7a3912d56f',1,'rapid_gauNS::rapid_gauNS()']]],
  ['rco_15',['RCO',['../class_r_c_o.html',1,'RCO'],['../class_r_c_o.html#ade4a63d4014191fc95d2121665a775fb',1,'RCO::RCO()']]],
  ['rco_16',['rco',['../structstarprint.html#abba39b47e679fb4cfb772e239cac9779',1,'starprint']]],
  ['rcore_17',['Rcore',['../class_star.html#aa6ecf956f618b7caba15cf838b43e94c',1,'Star']]],
  ['rd_18',['rd',['../class_i_o.html#a1440350ee5aa95d339103dd77df6b755',1,'IO']]],
  ['read_19',['read',['../class_i_o.html#a12be794007f9ae21f6d4a2ee922caa9d',1,'IO']]],
  ['read_5ftables_20',['read_tables',['../class_i_o.html#aee3cfa2b1755f02312b293fb4bb46f7a',1,'IO']]],
  ['readme_2emd_21',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['record_5fcondition_22',['record_condition',['../classevolve__utility_1_1_evolve_w1.html#a5c9f3a3d310a22cf48996e0f0bdab6e4',1,'evolve_utility::EvolveW1::record_condition()'],['../classevolve__utility_1_1_evolve_record_condition.html#a372f7cbb6d2de0b5b312f000591962db',1,'evolve_utility::EvolveRecordCondition::record_condition(_UNUSED Star &amp;star)'],['../classevolve__utility_1_1_evolve_record_condition.html#a691fb5549b667a4acae9c86be79f764c',1,'evolve_utility::EvolveRecordCondition::record_condition(_UNUSED Binstar &amp;binstar)'],['../classevolve__utility_1_1_evolve_b_h_m_s.html#abbef5ab9a0134b340be4e95f10dc6c16',1,'evolve_utility::EvolveBHMS::record_condition()'],['../classevolve__utility_1_1_evolve_r_r_l.html#ada2a427d22489aecb3005ec877b515b4',1,'evolve_utility::EvolveRRL::record_condition(Star &amp;star) override'],['../classevolve__utility_1_1_evolve_r_r_l.html#a933e0a0f4fc856fe008d6c1be16a4366',1,'evolve_utility::EvolveRRL::record_condition(Binstar &amp;binstar) override']]],
  ['recordstate_23',['recordstate',['../class_star.html#a49bf8080d0f536d90660022ea7b7acaf',1,'Star::recordstate()'],['../class_binstar.html#aed96353433af24141cac45949811c9a1',1,'Binstar::recordstate()']]],
  ['recordstate_5fbinary_24',['recordstate_binary',['../class_binstar.html#a0c5f61d5c97a0f193a24bd5315964989',1,'Binstar']]],
  ['recordstate_5fw_5ftimeupdate_25',['recordstate_w_timeupdate',['../class_star.html#a9ecf6903874752988930d8140b5affb0',1,'Star::recordstate_w_timeupdate()'],['../class_binstar.html#ad42962714c49b2201a94c9a80f7a9d21',1,'Binstar::recordstate_w_timeupdate()']]],
  ['register_26',['Register',['../class_orbital__change___s_n_kicks.html#ad2c47390cdd35bae409e8bbf6858ccab',1,'Orbital_change_SNKicks::Register()'],['../class_pair_instability.html#ad7d6eb2a3015d31e81d5879e46b573b0',1,'PairInstability::Register()'],['../class_binary_property.html#a3e52bf83ddde3616986ee48b5c7dfb38',1,'BinaryProperty::Register()'],['../class_m_tstability.html#a8172b95b56ad29b5d31eaf2499b24317',1,'MTstability::Register()'],['../class_orbital__change___tides.html#a2317048f0ca28750d7b8e9cbb01e9ee4',1,'Orbital_change_Tides::Register()'],['../class_orbital__change___g_w.html#aa4be476e5a40e9a68415ae915b0a7f8a',1,'Orbital_change_GW::Register()'],['../class_orbital__change___r_l.html#acb645abdbf15fdedbaae74e933321b10',1,'Orbital_change_RL::Register()'],['../class_orbital__change___mix.html#a59a35de94c46c3eeabae46c6a668e764',1,'Orbital_change_Mix::Register()'],['../class_orbital__change___c_e.html#a43de6b4f55e5dd91516ccde047ec6c99',1,'Orbital_change_CE::Register()'],['../class_process.html#a22775758327708f0135eb29387eb6e63',1,'Process::Register()'],['../class_kicks.html#a86dff21954db932117e2b4ff1146a192',1,'Kicks::Register()'],['../class_neutrino_mass_loss.html#a4b29264b9855051a83033f772b457dbd',1,'NeutrinoMassLoss::Register()'],['../classsupernova.html#a4daef0b63db7ae24820dabb5b7eec301',1,'supernova::Register()'],['../class_property.html#a6ced207ade04e475fe9efde9aa2bf2e2',1,'Property::Register()']]],
  ['register_5fspecific_27',['Register_specific',['../class_windaccretion.html#adb42931b22a56db7b78acebb7717041f',1,'Windaccretion::Register_specific()'],['../class_hardening.html#ad1c10dcddbeb177a9438ddb918b59e10',1,'Hardening::Register_specific()'],['../class_kollision.html#a0cf4b5dcff21b1f39750ae1f90063e25',1,'Kollision::Register_specific()'],['../class_circularisation.html#a01e2799921ef04246d433852551a570f',1,'Circularisation::Register_specific()']]],
  ['rel_5fdifference_28',['rel_difference',['../namespaceutilities.html#aed157133573e489cc7274532f561a5dd',1,'utilities']]],
  ['remnant_29',['remnant',['../class_star.html#a2949b020df6b5ff64453c5d7e05829d9',1,'Star']]],
  ['remnant_30',['Remnant',['../namespace_lookup.html#a340ca24ce16933d723ffeb25691f4fb2abf808233451b2bc93acce04764cd882d',1,'Lookup']]],
  ['remnant_2ecpp_31',['remnant.cpp',['../remnant_8cpp.html',1,'']]],
  ['remnant_2eh_32',['remnant.h',['../remnant_8h.html',1,'']]],
  ['remnant_5fin_5fbse_33',['remnant_in_bse',['../class_star.html#ab931b50049709130e90051824d056027',1,'Star']]],
  ['remnant_5fproperties_34',['remnant_properties',['../classsupernova.html#a308aecc1a743dfd89129206a7dcdb9ab',1,'supernova']]],
  ['remnant_5ftype_35',['remnant_type',['../classsupernova.html#a3a675cd751f07fbead0635e881a80eaf',1,'supernova::remnant_type()'],['../class_staremnant.html#a1f2ee7c0a66aabc56e7f038fcc97e033',1,'Staremnant::remnant_type()']]],
  ['remnants_36',['Remnants',['../namespace_lookup.html#a19e1f8d5a0039abbba0c2cb92145f4bc',1,'Lookup']]],
  ['remnanttype_37',['RemnantType',['../class_remnant_type.html#a42486f4f37ce48d7e1bf6fa438a29951',1,'RemnantType::RemnantType()'],['../class_remnant_type.html',1,'RemnantType']]],
  ['renvelope_38',['Renvelope',['../class_star.html#aabd1716d3b765414824917dc6267e8a5',1,'Star']]],
  ['repeated_5fevolution_39',['REPEATED_EVOLUTION',['../namespaceutilities.html#a5f79261e9e274d212bc3df7b41153218',1,'utilities']]],
  ['repeatstep_40',['repeatstep',['../class_binstar.html#a91908734182a6ef52aea045d8039725d',1,'Binstar::repeatstep()'],['../class_star.html#aa5d45c360b3b8e0792e8e79f6b383ece',1,'Star::repeatstep()']]],
  ['reset_41',['reset',['../class_property.html#aca3834d5693f263aea2d462b42d179db',1,'Property::reset()'],['../class_star.html#ad6342ca8df8ec77c6f08b101b3fc96f7',1,'Star::reset(double mzams=utilities::NULL_DOUBLE, double Z=utilities::NULL_DOUBLE, std::string tini=utilities::NULL_STR)'],['../class_star.html#aec56f6c481ec8663fbbc49c1331dafde',1,'Star::reset(double mzams=utilities::NULL_DOUBLE, double Z=utilities::NULL_DOUBLE, double tini=utilities::NULL_DOUBLE)'],['../class_lambda___nanjing.html#a5bb581d811d424265ed1133cf5c92e8d',1,'Lambda_Nanjing::reset()']]],
  ['reset_5fall_5fprocesses_5falarms_42',['reset_all_processes_alarms',['../class_binstar.html#a36128cfad5b054f6b33e5cd1ffbd1897',1,'Binstar']]],
  ['reset_5fdm_43',['reset_DM',['../class_orbital__change___r_l.html#a3f62e1fb1daae61308f02edd8a3ab538',1,'Orbital_change_RL']]],
  ['reset_5fdm_5fglobal_44',['reset_DM_global',['../class_roche_lobe.html#a94bc549f6656437600fbecf25bb520b1',1,'RocheLobe']]],
  ['reset_5fdmcumul_5frlo_5fin_5fstars_45',['reset_dMcumul_RLO_in_stars',['../class_roche_lobe.html#ac3ae7b6041f774d7ae1d0dee28b1d94a',1,'RocheLobe']]],
  ['reset_5fevent_46',['reset_event',['../class_process.html#aa770b4d8d9500a06caed88fa4b1a63d3',1,'Process']]],
  ['reset_5fevents_47',['reset_events',['../class_binstar.html#a8971009ead0cecde5352b5b8aeecfba3',1,'Binstar']]],
  ['reset_5fevolution_5fflags_48',['reset_evolution_flags',['../class_binstar.html#ae496c2e7acd865f56077d7eb3feed9cf',1,'Binstar']]],
  ['reset_5flog_49',['reset_log',['../class_i_o.html#a78259bd15f283fb29a0fd1b9d520736b',1,'IO']]],
  ['reset_5fqhe_50',['reset_QHE',['../class_star.html#af333cc3e66c66f4b762227fc93574c2f',1,'Star']]],
  ['reset_5fstaremnant_51',['reset_staremnant',['../class_star.html#a86f70b8787f98515ecef4505218ca863',1,'Star']]],
  ['reset_5fswallowed_52',['reset_swallowed',['../class_orbital__change___r_l.html#aea1b0d9c52aac3686dc70572cf543bd8',1,'Orbital_change_RL::reset_swallowed(int starID)'],['../class_orbital__change___r_l.html#aa068f080d138a0b832a2f8ddbb79b1d5',1,'Orbital_change_RL::reset_swallowed()']]],
  ['restore_53',['restore',['../class_binary_property.html#ad953650e2d58cb8e4e69124ff4dd364f',1,'BinaryProperty::restore()'],['../class_b_j_i_t___property.html#a2ea3b3c7c8a7d006b7696a35a5387b63',1,'BJIT_Property::restore()'],['../class_process.html#ad89d46b229414c2cd8077312b06e06cc',1,'Process::restore()'],['../class_property.html#a4781471dc24b8da09970e500604d73d9',1,'Property::restore()'],['../class_next_output.html#ad5d4204e6146f7b14590c3a8f208b79e',1,'NextOutput::restore()'],['../class_j_i_t___property.html#a4c7a4c724c401de5ee1ee569ecc6338f',1,'JIT_Property::restore()'],['../class_star.html#a44d531d2dff4805ba7491f0a2d544c7d',1,'Star::restore()']]],
  ['resynch_54',['resynch',['../class_b_timestep.html#add14ada665c499613a892cf7546f7633',1,'BTimestep::resynch()'],['../class_binstar.html#a1fb61a171833151e3f91c6b0ced607af',1,'Binstar::resynch()'],['../class_binary_property.html#a6ee50b550adecd1e2961f401e4478198',1,'BinaryProperty::resynch()'],['../class_property.html#a35b3b864c8fff60747f5019ea6f70c3a',1,'Property::resynch()'],['../class_timestep.html#a34b0dd43e3967e4aa258d1c3d0671a88',1,'Timestep::resynch(const double &amp;dt, bool set0) override'],['../class_timestep.html#a848c19ddced43d9b5b0f34db75e671e5',1,'Timestep::resynch(Star *s) override'],['../class_star.html#aa4fa622199a64ddca3098eeb72d77105',1,'Star::resynch()'],['../class_property.html#a77cf5606e51abbbe6c4bebcf1d150218',1,'Property::resynch()']]],
  ['rhe_55',['RHE',['../class_r_h_e.html#a6717d653bd386030ca969bf24ba4406f',1,'RHE::RHE()'],['../class_r_h_e.html',1,'RHE']]],
  ['rl_56',['RL',['../namespacestarparameter.html#a0b955e643744ae45bcc6bcd3ed9c3ae3a5716cbca66469410f40afdf172c3a49e',1,'starparameter']]],
  ['rl0_57',['RL0',['../class_r_l0.html#a2dbfb680bfcd48e217918eaf05b42c9b',1,'RL0::RL0()'],['../class_r_l0.html',1,'RL0']]],
  ['rl1_58',['RL1',['../class_r_l1.html#a80bb2ee3248646daf1287c12c5d8a235',1,'RL1']]],
  ['rl1_59',['rl1',['../class_hurley__rl.html#a60aad4cbb8770a37364b3c63f7ce1f9a',1,'Hurley_rl']]],
  ['rl1_60',['RL1',['../class_r_l1.html',1,'']]],
  ['rl_5feg_61',['RL_Eg',['../class___r_l.html#aea8950c20828edbef051ec9a394b5552',1,'_RL']]],
  ['rl_5ferror_62',['rl_error',['../classsevnstd_1_1rl__error.html#ae85fdcb6ec52ebf53fb3c38e14e0164f',1,'sevnstd::rl_error::rl_error()'],['../classsevnstd_1_1rl__error.html',1,'sevnstd::rl_error']]],
  ['rl_5fmode_63',['RL_mode',['../class_i_o.html#ac33e1ec6a721c92b6d7c057fbd71b3cc',1,'IO']]],
  ['rl_5fname_64',['RL_NAME',['../namespace_lookup.html#ac785d6e537426453874050f16adf39fa',1,'Lookup']]],
  ['rl_5fprimary_5ffinal_65',['RL_primary_final',['../class_common_envelope.html#a2c5a0d91144ce03f29e15c5238ea2966',1,'CommonEnvelope']]],
  ['rl_5fsecondary_5ffinal_66',['RL_secondary_final',['../class_common_envelope.html#af2e502dba5c54ac18fc4ee1041be9b48',1,'CommonEnvelope']]],
  ['rlmap_67',['RLMAP',['../namespace_lookup.html#a4ab0d50e2efe08ff9b26efa1c1018ddb',1,'Lookup']]],
  ['rlmap_68',['rlmap',['../namespace_lookup.html#ad23517187bf8c77c9febf03ad602055d',1,'Lookup']]],
  ['rlmap_5fname_69',['rlmap_name',['../namespace_lookup.html#af6e0689baa4894ea2136c8600d6d4783',1,'Lookup']]],
  ['rlmode_70',['RLMode',['../namespace_lookup.html#adb9c69058cb79d9adec3dc54145c95dc',1,'Lookup']]],
  ['rlo_71',['RLO',['../class_orbital__change___r_l.html#adb1e99472f7296e1f5e4f92c01c32ae5',1,'Orbital_change_RL::RLO()'],['../class_hurley__rl.html#a055ca0edd33300359c563d405c46e5d8',1,'Hurley_rl::RLO()']]],
  ['rlo_72',['rlo',['../namespaceutilities.html#ad1dc2a43fb623f48eb984a7c58e9e902',1,'utilities']]],
  ['rlo_5ffalse_73',['RLO_FALSE',['../namespaceutilities.html#a33e76025a31389355ff5d92499994b80',1,'utilities']]],
  ['rlo_5flast_5fstep_74',['RLO_last_step',['../class_roche_lobe.html#a46039c991322d0f95162dd45d7dc5995',1,'RocheLobe']]],
  ['rlo_5ftrue_75',['RLO_TRUE',['../namespaceutilities.html#a723786f902084fec38340605e9a2e5c7',1,'utilities']]],
  ['rlob_5fce_76',['RLOB_CE',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06a25cf211ffce56874b257de0acb28a4be',1,'Lookup']]],
  ['rlob_5fce_5fmerger_77',['RLOB_CE_Merger',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06a24f6d15efc0f5c6c3bf1aa91ce950632',1,'Lookup']]],
  ['rlob_5fmerger_78',['RLOB_Merger',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06abbd73e9fc6ad888bcd5e656e9d6c91c7',1,'Lookup']]],
  ['rlob_5fswallowed_79',['RLOB_Swallowed',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06a25556a5adf0be801e27223093856ebce',1,'Lookup']]],
  ['rlobegin_80',['RLOBegin',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06a030c29ea5f808c6ad6de948ea3648371',1,'Lookup']]],
  ['rloend_81',['RLOEnd',['../namespace_lookup.html#afe0c6d493f0ce9e41478a47bf12d4a06a976fe9393868b396611f2cce207d81f4',1,'Lookup']]],
  ['rloswapped_82',['RLOswapped',['../class_roche_lobe.html#a9c163ebf1c073aaed897a33217ac2394',1,'RocheLobe']]],
  ['rns_83',['Rns',['../class_n_srem.html#abe166432f487f7038776503c336f4788',1,'NSrem']]],
  ['roche_5flobe_5feg_84',['roche_lobe_Eg',['../namespaceutilities.html#afcca65bc160a7b86fecf33c7eb284365',1,'utilities']]],
  ['rochelobe_85',['RocheLobe',['../class_roche_lobe.html',1,'RocheLobe'],['../class_roche_lobe.html#a5400daa6dd61e1d8dee9b8e0fc951e89',1,'RocheLobe::RocheLobe()']]],
  ['root_5fdistribution_86',['root_distribution',['../class_n_srem.html#ab8aed379b7d7072e577bc12aab3c4ae6',1,'NSrem']]],
  ['rs_87',['Rs',['../class_rs.html',1,'Rs'],['../class_rs.html#a39e6b608ee84b673e3db63d273b4f394',1,'Rs::Rs()']]],
  ['rseed_88',['rseed',['../class_star.html#a095a0d8be60c671cb3c9beb9c4d3145a',1,'Star::rseed()'],['../class_binstar.html#a4adc381887e359e61cbc54dab6ffcec3',1,'Binstar::rseed()']]],
  ['rseed_5fprovided_89',['rseed_provided',['../class_i_o.html#a865cff47863a99f5e102ad9ef205db83',1,'IO']]],
  ['rsun_5fcgs_90',['Rsun_cgs',['../namespaceutilities.html#a4fbc9a2c262bad7fb3e8a278d6762213',1,'utilities']]],
  ['rzams_91',['rzams',['../class_star.html#a1497837df53291a47a8988099c0fea59',1,'Star']]],
  ['rzams0_92',['rzams0',['../class_star.html#a1f458d4c76b01c5f6e5d09a7d3bcd9b2',1,'Star']]],
  ['rzams_5fcached_93',['Rzams_cached',['../class_lambda.html#a5081009164eafa22e9156fb05830bcff',1,'Lambda']]]
];
