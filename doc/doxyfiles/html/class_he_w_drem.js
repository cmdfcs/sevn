var class_he_w_drem =
[
    [ "HeWDrem", "class_he_w_drem.html#a9f42407e1e6f7e9ebbe9c2a1f995dfe7", null ],
    [ "HeWDrem", "class_he_w_drem.html#a726e1c444930f2a62d377b36240d99bf", null ],
    [ "age", "class_he_w_drem.html#a81d90357225dd7b0e189413347733e8e", null ],
    [ "Bmag", "class_he_w_drem.html#a1e612ed91e697aa8649570b7b19710df", null ],
    [ "default_initialiser", "class_he_w_drem.html#a001035f1496801493a1e34fa7d434703", null ],
    [ "get", "class_he_w_drem.html#a4f0a63fddb7ee138f5c494af44132312", null ],
    [ "get_born_time", "class_he_w_drem.html#a3fb2eec3b62247ddec79d9256b3f6dfd", null ],
    [ "get_Mremnant_at_born", "class_he_w_drem.html#ac7592dbfe124d980ffa2589c23a54649", null ],
    [ "get_remnant_type", "class_he_w_drem.html#af172e833f7a2be034177e5ec3546760c", null ],
    [ "Inertia", "class_he_w_drem.html#a7a74d72d9c8df420e8c971fe3454814d", null ],
    [ "InertiaSphere", "class_he_w_drem.html#ab1a74d09d4b48efdddce170bbd06c1f8", null ],
    [ "Luminosity", "class_he_w_drem.html#a1f33da42e741238e7b581b5bf40f0d86", null ],
    [ "Mass", "class_he_w_drem.html#ae1217a0b00f428114665550c1acfaa23", null ],
    [ "OmegaRem", "class_he_w_drem.html#a51c81c4d63ba80e08c8c019240f08d65", null ],
    [ "Radius", "class_he_w_drem.html#a629a5f356fbca674c95b2baaae6df299", null ],
    [ "Xspin", "class_he_w_drem.html#a65e4397948b0c83f8c8b44aed3b7ae4f", null ],
    [ "A_luminosity", "class_he_w_drem.html#a7928996f1bbb90328aa5a00bad5acb53", null ],
    [ "born_time", "class_he_w_drem.html#a9b8ae00c6b853ca8420f2a6fa09950f8", null ],
    [ "Mremnant_at_born", "class_he_w_drem.html#a15460ef0ce7a97c3eea8b159ea9ff94a", null ],
    [ "remnant_type", "class_he_w_drem.html#a1f2ee7c0a66aabc56e7f038fcc97e033", null ],
    [ "svlog", "class_he_w_drem.html#a5d7579476b76d081c413d00b07595c67", null ]
];