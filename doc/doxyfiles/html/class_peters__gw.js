var class_peters__gw =
[
    [ "Peters_gw", "class_peters__gw.html#a1b432a4d6b7753efd13674646a6448c1", null ],
    [ "~Peters_gw", "class_peters__gw.html#ae00a1eebc6d0887e40cf21043b6ee545", null ],
    [ "DA", "class_peters__gw.html#a4e111d12dacae3b07b6a27111957c12b", null ],
    [ "DAngMomSpin", "class_peters__gw.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_peters__gw.html#a7832b8daaeeb0d5a778dc113c5268faa", null ],
    [ "DM", "class_peters__gw.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "function_core", "class_peters__gw.html#a247473a89b54d8cfd4cc4f42b6324b77", null ],
    [ "GetStaticMap", "class_peters__gw.html#a82ce4b3e4b18d1dcd9d56d58573c2898", null ],
    [ "GetUsed", "class_peters__gw.html#aa062cc01e0f5126b37179f2f9554ac94", null ],
    [ "init", "class_peters__gw.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_peters__gw.html#a834f1917b6d0b241ed0125b08812a964", null ],
    [ "Instance", "class_peters__gw.html#abfc6b84011ce3478d9a5b7c511047ba2", null ],
    [ "is_process_ongoing", "class_peters__gw.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_peters__gw.html#a343d2f401e614d37b97de667af5ce8ca", null ],
    [ "Peters_GW_da", "class_peters__gw.html#a993ec34937fdb29b2bd462145dd69931", null ],
    [ "Peters_GW_de", "class_peters__gw.html#a1173db0424a83d4ea360c1255f912c3a", null ],
    [ "Register", "class_peters__gw.html#aa4be476e5a40e9a68415ae915b0a7f8a", null ],
    [ "set_options", "class_peters__gw.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_peters__gw.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "_peters_gw", "class_peters__gw.html#aa383fd895e7e84cda9a4be6264bb49c3", null ],
    [ "c2_a", "class_peters__gw.html#acfdd2225d7cdb45a09d5a105279934db", null ],
    [ "c2_e", "class_peters__gw.html#aabf8ce844d47cafd1adae7f5c60942aa", null ],
    [ "c4_a", "class_peters__gw.html#a6a87072654da235151337d29579485c8", null ],
    [ "G3_over_c5", "class_peters__gw.html#a9799d2db550eafa379b6775655c0d0f7", null ],
    [ "K_a", "class_peters__gw.html#aeb2664922d99ef806ec2e58100b3af47", null ],
    [ "K_e", "class_peters__gw.html#aceb5e298d977799f78eab53521a81417", null ],
    [ "svlog", "class_peters__gw.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];