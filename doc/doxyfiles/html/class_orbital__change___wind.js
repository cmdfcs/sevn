var class_orbital__change___wind =
[
    [ "Orbital_change_Wind", "class_orbital__change___wind.html#ac51dcee37787298399aa506d03946ef7", null ],
    [ "~Orbital_change_Wind", "class_orbital__change___wind.html#a308916b6475b00ff4f713db34a571461", null ],
    [ "DA", "class_orbital__change___wind.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "class_orbital__change___wind.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_orbital__change___wind.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "class_orbital__change___wind.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "class_orbital__change___wind.html#a2f8118fb28c939655f9a1dff47f73a3d", null ],
    [ "GetUsed", "class_orbital__change___wind.html#a167f5aff25188a471f3ce86e061fe9dc", null ],
    [ "init", "class_orbital__change___wind.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_orbital__change___wind.html#aa9af92acfe270a7868d0639f8e9f01ce", null ],
    [ "Instance", "class_orbital__change___wind.html#a9b10015bec8cf096e139260c5eee4f6b", null ],
    [ "is_process_ongoing", "class_orbital__change___wind.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_orbital__change___wind.html#a11577fab56df78006f45e6cb543cd6ae", null ],
    [ "Register", "class_orbital__change___wind.html#aa8b054420b1078c9200866fdc5b79a1c", null ],
    [ "set_options", "class_orbital__change___wind.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_orbital__change___wind.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "svlog", "class_orbital__change___wind.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];