var hierarchy =
[
    [ "BinaryProperty", "class_binary_property.html", [
      [ "BEvent", "class_b_event.html", null ],
      [ "BTimestep", "class_b_timestep.html", null ],
      [ "BWorldtime", "class_b_worldtime.html", null ],
      [ "Derived_Property_Binary", "class_derived___property___binary.html", [
        [ "AngMom", "class_ang_mom.html", null ],
        [ "BJIT_Property", "class_b_j_i_t___property.html", null ],
        [ "GWtime", "class_g_wtime.html", null ],
        [ "Period", "class_period.html", null ],
        [ "_RL", "class___r_l.html", [
          [ "RL0", "class_r_l0.html", null ],
          [ "RL1", "class_r_l1.html", null ]
        ] ],
        [ "dadt", "classdadt.html", null ],
        [ "dedt", "classdedt.html", null ]
      ] ],
      [ "Eccentricity", "class_eccentricity.html", null ],
      [ "Semimajor", "class_semimajor.html", null ]
    ] ],
    [ "Binstar", "class_binstar.html", null ],
    [ "BSE_Coefficient", "class_b_s_e___coefficient.html", [
      [ "BSE_Coefficient_a1", "class_b_s_e___coefficient__a1.html", null ],
      [ "BSE_Coefficient_a10", "class_b_s_e___coefficient__a10.html", null ],
      [ "BSE_Coefficient_a2", "class_b_s_e___coefficient__a2.html", null ],
      [ "BSE_Coefficient_a3", "class_b_s_e___coefficient__a3.html", null ],
      [ "BSE_Coefficient_a4", "class_b_s_e___coefficient__a4.html", null ],
      [ "BSE_Coefficient_a5", "class_b_s_e___coefficient__a5.html", null ],
      [ "BSE_Coefficient_a6", "class_b_s_e___coefficient__a6.html", null ],
      [ "BSE_Coefficient_a7", "class_b_s_e___coefficient__a7.html", null ],
      [ "BSE_Coefficient_a8", "class_b_s_e___coefficient__a8.html", null ],
      [ "BSE_Coefficient_a9", "class_b_s_e___coefficient__a9.html", null ]
    ] ],
    [ "BSE_Property", "class_b_s_e___property.html", [
      [ "BSEtimes", "class_b_s_etimes.html", [
        [ "Tbgb", "class_tbgb.html", [
          [ "Thook", "class_thook.html", [
            [ "Tms", "class_tms.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "double3", "structdouble3.html", null ],
    [ "double4", "structdouble4.html", null ],
    [ "evolve_utility::EvolveFunctor", "classevolve__utility_1_1_evolve_functor.html", [
      [ "evolve_utility::EvolveBLC", "classevolve__utility_1_1_evolve_b_l_c.html", null ],
      [ "evolve_utility::EvolveBinaryCompactOld", "classevolve__utility_1_1_evolve_binary_compact_old.html", null ],
      [ "evolve_utility::EvolveDebug", "classevolve__utility_1_1_evolve_debug.html", null ],
      [ "evolve_utility::EvolveDefault", "classevolve__utility_1_1_evolve_default.html", null ],
      [ "evolve_utility::EvolveRecordCondition", "classevolve__utility_1_1_evolve_record_condition.html", [
        [ "evolve_utility::EvolveBHMS", "classevolve__utility_1_1_evolve_b_h_m_s.html", null ],
        [ "evolve_utility::EvolveRRL", "classevolve__utility_1_1_evolve_r_r_l.html", null ],
        [ "evolve_utility::EvolveW1", "classevolve__utility_1_1_evolve_w1.html", null ]
      ] ],
      [ "evolve_utility::EvolveStopCondition", "classevolve__utility_1_1_evolve_stop_condition.html", [
        [ "evolve_utility::EvolveBBH", "classevolve__utility_1_1_evolve_b_b_h.html", null ],
        [ "evolve_utility::EvolveBCX1", "classevolve__utility_1_1_evolve_b_c_x1.html", null ],
        [ "evolve_utility::EvolveBHMSTarantula", "classevolve__utility_1_1_evolve_b_h_m_s_tarantula.html", null ],
        [ "evolve_utility::EvolveBinaryCompact", "classevolve__utility_1_1_evolve_binary_compact.html", null ]
      ] ]
    ] ],
    [ "std::exception", null, [
      [ "sevnstd::sevnerr", "classsevnstd_1_1sevnerr.html", [
        [ "sevnstd::bse_error", "classsevnstd_1_1bse__error.html", [
          [ "sevnstd::ce_error", "classsevnstd_1_1ce__error.html", null ],
          [ "sevnstd::rl_error", "classsevnstd_1_1rl__error.html", null ]
        ] ],
        [ "sevnstd::jtrack_error", "classsevnstd_1_1jtrack__error.html", null ],
        [ "sevnstd::notimplemented_error", "classsevnstd_1_1notimplemented__error.html", null ],
        [ "sevnstd::params_error", "classsevnstd_1_1params__error.html", null ],
        [ "sevnstd::sanity_error", "classsevnstd_1_1sanity__error.html", null ],
        [ "sevnstd::sevnio_error", "classsevnstd_1_1sevnio__error.html", null ],
        [ "sevnstd::sse_error", "classsevnstd_1_1sse__error.html", [
          [ "sevnstd::sn_error", "classsevnstd_1_1sn__error.html", null ]
        ] ]
      ] ]
    ] ],
    [ "IO", "class_i_o.html", null ],
    [ "Kicks", "class_kicks.html", [
      [ "ECUS30", "class_e_c_u_s30.html", null ],
      [ "Hobbs", "class_hobbs.html", [
        [ "CC15", "class_c_c15.html", null ],
        [ "EC15CC265", "class_e_c15_c_c265.html", null ]
      ] ],
      [ "HobbsPure", "class_hobbs_pure.html", null ],
      [ "Unified", "class_unified.html", null ],
      [ "Zeros", "class_zeros.html", null ]
    ] ],
    [ "Lambda_Base", "class_lambda___base.html", [
      [ "Lambda_Klencki", "class_lambda___klencki.html", [
        [ "Lambda_Klencki_interpolator", "class_lambda___klencki__interpolator.html", null ]
      ] ],
      [ "Lambda_Nanjing", "class_lambda___nanjing.html", [
        [ "Lambda_Nanjing_interpolator", "class_lambda___nanjing__interpolator.html", null ]
      ] ]
    ] ],
    [ "utilities::ListGenerator", "classutilities_1_1_list_generator.html", null ],
    [ "utilities::MassContainer", "structutilities_1_1_mass_container.html", null ],
    [ "MTstability", "class_m_tstability.html", [
      [ "MT_Qcrit", "class_m_t___qcrit.html", [
        [ "Qcrit_Hurley", "class_qcrit___hurley.html", [
          [ "Qcrit_Hurley_Webbink", "class_qcrit___hurley___webbink.html", [
            [ "Qcrit_COSMIC_Claeys", "class_qcrit___c_o_s_m_i_c___claeys.html", null ],
            [ "Qcrit_COSMIC_Neijssel", "class_qcrit___c_o_s_m_i_c___neijssel.html", null ],
            [ "Qcrit_HRadiative_Stable", "class_qcrit___h_radiative___stable.html", null ],
            [ "Qcrit_Hurley_Webbink_Shao", "class_qcrit___hurley___webbink___shao.html", null ],
            [ "Qcrit_Radiative_Stable", "class_qcrit___radiative___stable.html", null ]
          ] ]
        ] ],
        [ "Qcrit_StarTrack", "class_qcrit___star_track.html", null ]
      ] ],
      [ "MT_Stable", "class_m_t___stable.html", null ],
      [ "MT_UnStable", "class_m_t___un_stable.html", null ],
      [ "MT_Zeta", "class_m_t___zeta.html", null ]
    ] ],
    [ "NeutrinoMassLoss", "class_neutrino_mass_loss.html", [
      [ "NMLDisabled", "class_n_m_l_disabled.html", null ],
      [ "NMLLattimer89", "class_n_m_l_lattimer89.html", null ]
    ] ],
    [ "evolve_utility::Options", "structevolve__utility_1_1_options.html", null ],
    [ "Orbital_change", "class_orbital__change.html", [
      [ "Orbital_change_CE", "class_orbital__change___c_e.html", [
        [ "disabled_CE", "classdisabled___c_e.html", null ],
        [ "energy_CE", "classenergy___c_e.html", null ]
      ] ],
      [ "Orbital_change_GW", "class_orbital__change___g_w.html", [
        [ "Peters_gw", "class_peters__gw.html", null ],
        [ "disabled_gw", "classdisabled__gw.html", null ]
      ] ],
      [ "Orbital_change_Mix", "class_orbital__change___mix.html", [
        [ "disabled_mix", "classdisabled__mix.html", null ],
        [ "simple_mix", "classsimple__mix.html", null ]
      ] ],
      [ "Orbital_change_RL", "class_orbital__change___r_l.html", [
        [ "Hurley_rl", "class_hurley__rl.html", [
          [ "Hurley_mod_rl", "class_hurley__mod__rl.html", null ],
          [ "Hurley_rl_bse", "class_hurley__rl__bse.html", null ],
          [ "Hurley_rl_propeller", "class_hurley__rl__propeller.html", null ]
        ] ],
        [ "disabled_rl", "classdisabled__rl.html", null ]
      ] ],
      [ "Orbital_change_SNKicks", "class_orbital__change___s_n_kicks.html", [
        [ "Hurley_SNKicks", "class_hurley___s_n_kicks.html", null ],
        [ "disabled_SNKicks", "classdisabled___s_n_kicks.html", null ]
      ] ],
      [ "Orbital_change_Tides", "class_orbital__change___tides.html", [
        [ "Tides_simple", "class_tides__simple.html", [
          [ "Tides_simple_notab", "class_tides__simple__notab.html", null ]
        ] ],
        [ "disabled_tides", "classdisabled__tides.html", null ]
      ] ]
    ] ],
    [ "PairInstability", "class_pair_instability.html", [
      [ "PIDisabled", "class_p_i_disabled.html", null ],
      [ "PIFarmer19", "class_p_i_farmer19.html", null ],
      [ "PIIorio22", "class_p_i_iorio22.html", [
        [ "PIIorio22Limited", "class_p_i_iorio22_limited.html", null ]
      ] ],
      [ "PIMapelli20", "class_p_i_mapelli20.html", null ]
    ] ],
    [ "PISNreturn", "struct_p_i_s_nreturn.html", null ],
    [ "Process", "class_process.html", [
      [ "Circularisation", "class_circularisation.html", [
        [ "CircularisationDisabled", "class_circularisation_disabled.html", null ],
        [ "StandardCircularisation", "class_standard_circularisation.html", [
          [ "CircularisationAngMom", "class_circularisation_ang_mom.html", null ],
          [ "CircularisationPeriastron", "class_circularisation_periastron.html", [
            [ "CircularisationPeriastronFull", "class_circularisation_periastron_full.html", null ]
          ] ],
          [ "CircularisationSemimajor", "class_circularisation_semimajor.html", null ]
        ] ]
      ] ],
      [ "GWrad", "class_g_wrad.html", null ],
      [ "Hardening", "class_hardening.html", [
        [ "HardeningDisabled", "class_hardening_disabled.html", null ],
        [ "HardeningFastCluster", "class_hardening_fast_cluster.html", null ]
      ] ],
      [ "Kollision", "class_kollision.html", [
        [ "KollisionDisabled", "class_kollision_disabled.html", null ],
        [ "KollisionHurley", "class_kollision_hurley.html", null ]
      ] ],
      [ "MaccretionProcess", "class_maccretion_process.html", [
        [ "CommonEnvelope", "class_common_envelope.html", null ],
        [ "RocheLobe", "class_roche_lobe.html", null ],
        [ "Windaccretion", "class_windaccretion.html", [
          [ "WindaccretionDisabled", "class_windaccretion_disabled.html", null ],
          [ "WindaccretionHurley", "class_windaccretion_hurley.html", null ]
        ] ]
      ] ],
      [ "Mix", "class_mix.html", null ],
      [ "SNKicks", "class_s_n_kicks.html", null ],
      [ "Tides", "class_tides.html", null ]
    ] ],
    [ "Property", "class_property.html", [
      [ "AngMomSpin", "class_ang_mom_spin.html", null ],
      [ "Bmag", "class_bmag.html", null ],
      [ "Derivative_Property", "class_derivative___property.html", [
        [ "dMCOdt", "classd_m_c_odt.html", null ],
        [ "dMHEdt", "classd_m_h_edt.html", null ],
        [ "dMdt", "classd_mdt.html", null ],
        [ "dRdt", "classd_rdt.html", null ]
      ] ],
      [ "Derived_Property", "class_derived___property.html", [
        [ "JIT_Property", "class_j_i_t___property.html", [
          [ "Ebind", "class_ebind.html", null ],
          [ "Event", "class_event.html", null ],
          [ "Lambda", "class_lambda.html", null ],
          [ "NSsalpha", "class_n_ssalpha.html", null ],
          [ "PhaseBSE", "class_phase_b_s_e.html", null ],
          [ "Plife", "class_plife.html", null ],
          [ "Zams", "class_zams.html", null ],
          [ "Zmet", "class_zmet.html", null ],
          [ "dMRLOdt", "classd_m_r_l_odt.html", null ],
          [ "dMaccwinddt", "classd_maccwinddt.html", null ]
        ] ],
        [ "OmegaSpin", "class_omega_spin.html", null ],
        [ "Rs", "class_rs.html", null ],
        [ "Spin", "class_spin.html", null ],
        [ "Temperature", "class_temperature.html", null ],
        [ "Xspin", "class_xspin.html", null ]
      ] ],
      [ "OmegaRem", "class_omega_rem.html", null ],
      [ "RemnantType", "class_remnant_type.html", null ],
      [ "TableProperty", "class_table_property.html", [
        [ "Luminosity", "class_luminosity.html", null ],
        [ "Mass_obejct", "class_mass__obejct.html", [
          [ "MCO", "class_m_c_o.html", null ],
          [ "MHE", "class_m_h_e.html", null ],
          [ "Mass", "class_mass.html", null ]
        ] ],
        [ "OptionalTableProperty", "class_optional_table_property.html", [
          [ "ConvectiveTable", "class_convective_table.html", [
            [ "Depthconv", "class_depthconv.html", null ],
            [ "Qconv", "class_qconv.html", null ],
            [ "Tconv", "class_tconv.html", null ]
          ] ],
          [ "CoreRadius", "class_core_radius.html", [
            [ "RCO", "class_r_c_o.html", null ],
            [ "RHE", "class_r_h_e.html", null ]
          ] ],
          [ "Inertia", "class_inertia.html", null ],
          [ "SurfaceAbundanceTable", "class_surface_abundance_table.html", [
            [ "Csup", "class_csup.html", null ],
            [ "HEsup", "class_h_esup.html", null ],
            [ "Hsup", "class_hsup.html", null ],
            [ "Nsup", "class_nsup.html", null ],
            [ "Osup", "class_osup.html", null ]
          ] ]
        ] ],
        [ "Phase", "class_phase.html", null ],
        [ "R_object", "class_r__object.html", [
          [ "CoreRadius", "class_core_radius.html", null ],
          [ "Radius", "class_radius.html", null ]
        ] ]
      ] ],
      [ "Time_object", "class_time__object.html", [
        [ "Localtime", "class_localtime.html", null ],
        [ "NextOutput", "class_next_output.html", null ],
        [ "Timestep", "class_timestep.html", null ],
        [ "Worldtime", "class_worldtime.html", null ]
      ] ],
      [ "dMcumul_RLO", "classd_mcumul___r_l_o.html", null ],
      [ "dMcumul_binary", "classd_mcumul__binary.html", null ],
      [ "dMcumulacc_wind", "classd_mcumulacc__wind.html", null ]
    ] ],
    [ "SEVNinfo", "struct_s_e_v_ninfo.html", null ],
    [ "sevnstd::SevnLogging", "classsevnstd_1_1_sevn_logging.html", null ],
    [ "SEVNpar", "class_s_e_v_npar.html", null ],
    [ "Star", "class_star.html", [
      [ "Empty", "class_empty.html", null ],
      [ "Star_auxiliary", "class_star__auxiliary.html", null ]
    ] ],
    [ "Staremnant", "class_staremnant.html", [
      [ "BHrem", "class_b_hrem.html", null ],
      [ "NSrem", "class_n_srem.html", [
        [ "NSCCrem", "class_n_s_c_crem.html", null ],
        [ "NSECrem", "class_n_s_e_crem.html", null ]
      ] ],
      [ "WDrem", "class_w_drem.html", [
        [ "COWDrem", "class_c_o_w_drem.html", null ],
        [ "HeWDrem", "class_he_w_drem.html", null ],
        [ "ONeWDrem", "class_o_ne_w_drem.html", null ]
      ] ],
      [ "Zombierem", "class_zombierem.html", null ]
    ] ],
    [ "starprint", "structstarprint.html", null ],
    [ "supernova", "classsupernova.html", [
      [ "DeathMatrix", "class_death_matrix.html", null ],
      [ "NSfromGau", "class_n_sfrom_gau.html", [
        [ "delayed_gauNS", "classdelayed__gau_n_s.html", null ],
        [ "rapid_gauNS", "classrapid__gau_n_s.html", null ]
      ] ],
      [ "NeutrinoMassLossOFF", "class_neutrino_mass_loss_o_f_f.html", [
        [ "DeathMatrix", "class_death_matrix.html", null ],
        [ "directcollapse", "classdirectcollapse.html", null ]
      ] ],
      [ "PisnOFF", "class_pisn_o_f_f.html", [
        [ "DeathMatrix", "class_death_matrix.html", null ],
        [ "directcollapse", "classdirectcollapse.html", null ]
      ] ],
      [ "PisnON", "class_pisn_o_n.html", [
        [ "compactness", "classcompactness.html", null ],
        [ "delayed", "classdelayed.html", [
          [ "delayed_gauNS", "classdelayed__gau_n_s.html", null ]
        ] ],
        [ "rapid", "classrapid.html", [
          [ "rapid_gauNS", "classrapid__gau_n_s.html", null ]
        ] ]
      ] ],
      [ "compactness", "classcompactness.html", null ],
      [ "delayed", "classdelayed.html", null ],
      [ "directcollapse", "classdirectcollapse.html", null ],
      [ "rapid", "classrapid.html", null ]
    ] ]
];